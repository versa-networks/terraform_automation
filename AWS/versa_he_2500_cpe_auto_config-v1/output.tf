output "DC_output_details" {
  value = "${module.versa_he_2500_cpe_dc}"
}
output "DR_output_details" {
  value = "${module.versa_he_2500_cpe_dr}"
}