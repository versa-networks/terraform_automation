#provider INFO
variable "dc_region" {
  description = "Specifie region for DC HE creation "
}
variable "dr_region" {
  description = "Specifie region for DR HE creation "
}
variable "access_key" {
    description = "Access key of AWS account to be used to deploy VM on AWS."
}
variable "secret_key" {
    description =  "Secret key of AWS account to be used to deploy VM on AWS."
}
variable "tag_name" {
  description = "tag Name to identify the HE"
}

variable "key_pair_name" {
    description = "key pair name for device login"
}

variable "key_pair_file_path" {
  description = "Key file path"
}

variable "hostname_dir_1" {
  description = "Director host name"
  # default = "versa-director-1"
}
variable "hostname_dir_2" {
  description = "Director host name"
  # default = "versa-director-2"
}
variable "analytics_1_hostname" {
  description = "analytics hostname"
  # default = "van-analytics-01"
}
variable "search_1_hostname" {
  description = "search hostname"
  # default = "van-search-01"
}
variable "analytics_2_hostname" {
  description = "analytics hostname"
  # default = "van-analytics-01"
}
variable "search_2_hostname" {
  description = "search hostname"
  # default = "van-search-01"
}
variable "controller_1_hostname" {
  description = "controller 1 host name"
  # default = "Controller-1"  
}
variable "controller_1_country" {
  description = "controller 1 country name"
  # default = "Singapore"  
}
variable "controller_2_hostname" {
  description = "controller 2 host name"
  # default = "Controller-2"  
}
variable "controller_2_country" {
  description = "controller 2 country name"
  # default = "california"  
}
#DC-DR AMI and INSTANCE TYPE

variable "DC_Director_ami" {
    description = "AMI Image to be used to deploy versa director in DC"
}
variable "DR_Director_ami" {
    description = "AMI Image to be used to deploy versa director in DR"
}
variable "Director_instance_type" {
    description = "Type of Ec2 instance for director"
}

variable "DC_controller_ami" {
    description = "AMI Image to be used to deploy controller in DC"
}
variable "DR_controller_ami" {
    description = "AMI Image to be used to deploy controller in DR"
}

variable "controller_instance_type" {
    description = "Type of Ec2 instance for controller"
}

variable "router_instance_type" {
    description = "Type of Ec2 instance for routers in DC and DR"
}

variable "DC_analytics_ami" {
    description = "AMI Image to be used to deploy versa analytics in DC"
}

variable "analytics_instance_type" {
    description = "Type of Ec2 instance for analytics"
}

#DC-DR CIDR Subnet INFO

variable "DC_cidr_block" {
    description = "IPV4 CIDR for VPC creation in DC"
    # default = "10.153.0.0/16"
}
variable "DR_cidr_block" {
    description = "IPV4 CIDR for VPC creation in DR"
    # default = "10.193.0.0/16"
}
#DC-DR CIDR MGNT Subnet INFO
#### DC CIDR MGNT Subnet INFO ####
variable "DC_mgmt_subnet" {
  description = "Management Subnet for VM in AWS DC"
  # default = "10.153.0.0/24"
}
variable "DC_mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.153.0.1"
}
variable "DC_dir_ana_mgnt_interfaces_IP" {
  # default = ["10.153.0.21","10.153.0.25","10.153.0.27","10.153.0.26","10.153.0.28"]
} 
variable "DC_controller_flex_mgnt_interfaces_IP" {
  # default = ["10.153.0.22","10.153.0.23","10.153.0.24"]
}
#### DR CIDR MGNT Subnet INFO ####
variable "DR_mgmt_subnet" {
  description = "Management Subnet for VM in AWS DR"
  # default = "10.193.0.0/24"
}
variable "DR_mgmt_subnet_gateway" {
  description = "Management Ip Gateway for director northbound"
  # default = "10.193.0.1"
}
variable "DR_dir_ana_mgnt_interfaces_IP" {
  # default = ["10.193.0.21"]
} 
variable "DR_controller_flex_mgnt_interfaces_IP" {
  # default = ["10.193.0.22","10.193.0.23","10.193.0.24"]
}
#DC-DR CIDR INTERNET Subnet INFO
#### DC CIDR INTERNET Subnet INFO ####
variable "DC_internet_subnet" {
  description = "Internet Subnet for VM in AWS DC"
  # default = "10.153.1.0/24"
}
variable "DC_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.153.1.1"
}

variable "DC_controller_flex_internet_network_interfaces_IP" {
  # default = ["10.153.1.22","10.153.1.23"]
}
#### DR CIDR INTERNET Subnet INFO ####
variable "DR_internet_subnet" {
  description = "Internet Subnet for VM in AWS DR"
  # default = "10.193.1.0/24"
}
variable "DR_internet_subnet_gateway" {
  description = "Internet Gateway for internet rechability"
  # default = "10.193.1.1"
}
variable "DR_controller_flex_internet_network_interfaces_IP" {
  # default = ["10.193.1.22","10.193.1.23"]
}
#DC-DR CIDR SOUTH_BOUND Subnet INFO
#### DC CIDR SOUTH_BOUND Subnet INFO ####
variable "DC_south_bound_network_subnet" {
  description = "control network subnet for VM in AWS DC"
  # default = "10.153.2.0/24"
}
variable "DC_south_bound_network_interfaces_IP" {
  # default = ["10.153.2.21","10.153.2.25","10.153.2.27","10.153.2.26","10.153.2.28","10.153.2.22","10.153.2.23"]
}
#### DR CIDR SOUTH_BOUND Subnet INFO ####
variable "DR_south_bound_network_subnet" {
  description = "control network subnet for VM in AWS DR"
  # default = "10.193.2.0/24"
}
variable "DR_south_bound_network_interfaces_IP" {
  # default = ["10.193.2.21","10.193.2.22","10.193.2.23"]
} 
#DC-DR PUBLIC ACCESS Subnet INFO
variable "Public_subnet_resource_access" {
  description = "Define public IP to access the resources"
#   default = "103.77.37.189/32"
}
variable "parent_org_name" {
  description = "Define org to auto_deployment"
#   default = "versa"
}
variable "overlay_prefixes" {
  description = "overlay prefixes"
  # default = "10.0.0.0/8"
}

