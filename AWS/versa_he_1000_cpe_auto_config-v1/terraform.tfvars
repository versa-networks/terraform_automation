
#provider INFO
dc_region = "ap-southeast-1"
dr_region = "us-west-1"
access_key = "AKIA5MZGWGX2EJBBNUMKXXXXXXXXXXXXXX"
secret_key = "qYqCa1sWrZSmDwOUl+4uM9uyESKqffjdy8TKbvSGXXXXXXXXXXXXXX"
tag_name = "customer-1"

#file path example /Users/dinz/Downloads
key_pair_file_path = "/Users/dinz/Downloads"
key_pair_name = "dkumar"

#Host Name for Director
hostname_dir_1 = "cloud-director-1"
hostname_dir_2 = "cloud-director-2"
analytics_1_hostname = "van-analytics-01"
search_1_hostname = "van-search-01"
analytics_2_hostname = "van-analytics-02"
search_2_hostname = "van-search-02"
controller_1_hostname = "Controller-1"
controller_1_country = "Singapore"
controller_2_hostname = "Controller-2"
controller_2_country = "california"

#DC and DR INSTANCE TYPE
### LAB ###
# Director_instance_type = "c5.xlarge"
# analytics_instance_type = "c5.xlarge"
# controller_instance_type = "c5.xlarge"
# router_instance_type = "c5.xlarge"

### PRODUCTION ###
Director_instance_type = "c5.4xlarge"
analytics_instance_type = "c5.4xlarge"
controller_instance_type = "c5.4xlarge"
router_instance_type = "c5.xlarge"

# # DC 21.2.1 Trusty ap-southeast-1

DC_Director_ami = "ami-0c415f20ff1288b97"
DC_analytics_ami = "ami-03f33db7b010f507f"
DC_controller_ami = "ami-02a2178c259bf0b91"

# # DR 21.2.1 Trusty us-west-1

DR_Director_ami = "ami-0999b44537aa8a55e"
DR_controller_ami = "ami-03db2d561667486a8"

#DC-DR CIDR Subnet INFO

DC_cidr_block = "10.153.0.0/16"
DR_cidr_block = "10.193.0.0/16"

#DC-DR CIDR MGNT Subnet INFO
#### DC MGNT Subnet INFO ####
DC_mgmt_subnet = "10.153.0.0/24"
DC_mgmt_subnet_gateway = "10.153.0.1"
#["Master_Director","Analytics_1","Search_1","Analytics_2","Search_2"]
DC_dir_ana_mgnt_interfaces_IP = ["10.153.0.21","10.153.0.25","10.153.0.27","10.153.0.26","10.153.0.28"]
#[DC_router,Controller1,DC_DR_MGNT]
DC_controller_flex_mgnt_interfaces_IP =  ["10.153.0.22","10.153.0.23","10.153.0.24"]

#### DR CIDR MGNT Subnet INFO ####
DR_mgmt_subnet = "10.193.0.0/24"
DR_mgmt_subnet_gateway = "10.193.0.1"
#[Slave_Director]
DR_dir_ana_mgnt_interfaces_IP = ["10.193.0.21"]
#[DR_router,Controller2,DR_DC_MGNT]
DR_controller_flex_mgnt_interfaces_IP = ["10.193.0.22","10.193.0.23","10.193.0.24"]
#DC-DR CIDR INTERNET Subnet INFO
#### DC INTERNET Subnet INFO ####
DC_internet_subnet = "10.153.1.0/24"
DC_internet_subnet_gateway = "10.153.1.1"
#[DC_router,Controller1]
DC_controller_flex_internet_network_interfaces_IP = ["10.153.1.22","10.153.1.23"]

#### DR CIDR INTERNET Subnet INFO ####
DR_internet_subnet = "10.193.1.0/24"
DR_internet_subnet_gateway = "10.193.1.1"
#[DR_router,Controller2]
DR_controller_flex_internet_network_interfaces_IP = ["10.193.1.22","10.193.1.23"]

#DC-DR CIDR SOUTH_BOUND Subnet INFO
#### DC CIDR SOUTH_BOUND Subnet INFO ####

DC_south_bound_network_subnet = "10.153.2.0/24"
#["Master_Director","Analytics_1","Search_1","Analytics_2","Search_2",DC_router,Controller1]
DC_south_bound_network_interfaces_IP = ["10.153.2.21","10.153.2.25","10.153.2.27","10.153.2.26","10.153.2.28","10.153.2.22","10.153.2.23"]

#### DR CIDR SOUTH_BOUND Subnet INFO ####

DR_south_bound_network_subnet = "10.193.2.0/24"
#[Slave_Director,DR_router,Controller2]
DR_south_bound_network_interfaces_IP = ["10.193.2.21","10.193.2.22","10.193.2.23"]

#Accessable subnet
Public_subnet_resource_access ="0.0.0.0/0"

#ORG NAME
parent_org_name = "versa"

overlay_prefixes = "10.0.0.0/8"
