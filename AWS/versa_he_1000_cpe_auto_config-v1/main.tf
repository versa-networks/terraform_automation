module "versa_he_1000_cpe_dc" {
  source = "./versa_he_1000_cpe_multiregion_dc"
  region = var.dc_region
  access_key = var.access_key
  secret_key = var.secret_key
  tag_name = var.tag_name
  key_pair_file_path = var.key_pair_file_path
  key_pair_name = var.key_pair_name
  #hostnames
  hostname_dir_1 = var.hostname_dir_1
  hostname_dir_2 = var.hostname_dir_2
  analytics_1_hostname = var.analytics_1_hostname
  search_1_hostname = var.search_1_hostname    
  analytics_2_hostname = var.analytics_2_hostname
  search_2_hostname = var.search_2_hostname    
  controller_1_hostname = var.controller_1_hostname
  controller_1_country = var.controller_1_country
  controller_2_hostname = var.controller_2_hostname
  controller_2_country = var.controller_2_country
  #DC AMI INSTANCE TYPE  
  Director_ami = var.DC_Director_ami
  Director_instance_type = var.Director_instance_type
  analytics_ami = var.DC_analytics_ami
  analytics_instance_type = var.analytics_instance_type
  controller_ami = var.DC_controller_ami
  controller_instance_type = var.controller_instance_type
  dc_router_instance_type = var.router_instance_type
  #DC Subnet INFO  
  cidr_block = var.DC_cidr_block
  #MGNT Subnet INFO
  mgmt_subnet = var.DC_mgmt_subnet
  mgmt_subnet_gateway = var.DC_mgmt_subnet_gateway
  DR_mgmt_subnet = var.DR_mgmt_subnet
  dir_ana_mgnt_interfaces_IP = var.DC_dir_ana_mgnt_interfaces_IP
  controller_flex_mgnt_interfaces_IP = var.DC_controller_flex_mgnt_interfaces_IP
  #INTERNET Subnet INFO
  internet_subnet = var.DC_internet_subnet
  DC_internet_subnet_gateway = var.DC_internet_subnet_gateway
  controller_flex_internet_network_interfaces_IP = var.DC_controller_flex_internet_network_interfaces_IP
  #SOUTH_BOUND Subnet INFO
  south_bound_network_subnet = var.DC_south_bound_network_subnet
  south_bound_network_interfaces_IP = var.DC_south_bound_network_interfaces_IP
  #Accessable subnet
  Public_subnet_resource_access = var.Public_subnet_resource_access
  slave_dir_mgmt_ip = module.versa_he_1000_cpe_dr.slave_director_mgnt_interface_private_ip
  slave_dir_south_bound_ip = module.versa_he_1000_cpe_dr.slave_director_south_bound_network_interface_private_ip
  DR_internet_public_IP = module.versa_he_1000_cpe_dr.dr_router_internet_interface_public_ip
  DR_internet_private_IP = module.versa_he_1000_cpe_dr.dr_router_internet_interface_private_ip
  DR_router_eth0_mgnt_ip = module.versa_he_1000_cpe_dr.dr_router_mgnt_interface_private_ip
  #ORG Name
  parent_org_name = var.parent_org_name
  overlay_prefixes = var.overlay_prefixes
  #Controller_2 config
  controller_2_mgnt_ip = module.versa_he_1000_cpe_dr.controller_2_mgnt_interface_private_ip
  controller_2_south_bound_subent = var.DR_south_bound_network_subnet
  controller_2_internet_subent = var.DR_internet_subnet
  DR_internet_subnet_gateway = var.DR_internet_subnet_gateway
  controller_2_south_bound_ip = module.versa_he_1000_cpe_dr.controller_2_south_bound_network_interface_private_ip
  controller_2_internet_private_ip = module.versa_he_1000_cpe_dr.controller_2_internet_interface_private_ip
  controller_2_internet_public_ip = module.versa_he_1000_cpe_dr.controller_2_internet_interface_public_ip
  dr_router_south_bound_ip = module.versa_he_1000_cpe_dr.dr_router_south_bound_network_interface_private_ip
}

module "versa_he_1000_cpe_dr" {
  source = "./versa_he_1000_cpe_multiregion_dr"
  region = var.dr_region
  access_key = var.access_key
  secret_key = var.secret_key  
  tag_name = var.tag_name
  key_pair_file_path = var.key_pair_file_path
  key_pair_name = var.key_pair_name
  hostname_dir_1 = var.hostname_dir_1
  hostname_dir_2 = var.hostname_dir_2  
  #DR AMI INSTANCE TYPE  
  Director_ami = var.DR_Director_ami  
  Director_instance_type = var.Director_instance_type
  controller_ami = var.DR_controller_ami  
  controller_instance_type = var.controller_instance_type
  dr_router_instance_type = var.router_instance_type
  #DR Subnet INFO   
  cidr_block = var.DR_cidr_block
  #MGNT Subnet INFO
  mgmt_subnet = var.DR_mgmt_subnet
  mgmt_subnet_gateway = var.DR_mgmt_subnet_gateway  
  DC_mgmt_subnet = var.DC_mgmt_subnet  
  dir_ana_mgnt_interfaces_IP = var.DR_dir_ana_mgnt_interfaces_IP
  controller_flex_mgnt_interfaces_IP = var.DR_controller_flex_mgnt_interfaces_IP 
  #INTERNET Subnet INFO   
  internet_subnet = var.DR_internet_subnet
  controller_flex_internet_network_interfaces_IP = var.DR_controller_flex_internet_network_interfaces_IP
  #SOUTH_BOUND Subnet INFO  
  south_bound_network_subnet = var.DR_south_bound_network_subnet
  south_bound_network_interfaces_IP = var.DR_south_bound_network_interfaces_IP
  #Accessable subnet
  Public_subnet_resource_access = var.Public_subnet_resource_access  
  #Script INFO
  master_dir_mgmt_ip = module.versa_he_1000_cpe_dc.master_director_mgnt_interface_private_ip
  master_dir_south_bound_ip = module.versa_he_1000_cpe_dc.master_director_south_bound_network_interface_private_ip
  DC_internet_public_IP = module.versa_he_1000_cpe_dc.dc_router_internet_interface_public_ip
  DC_internet_private_IP = module.versa_he_1000_cpe_dc.dc_router_internet_interface_private_ip
  DR_internet_subnet_gateway = var.DR_internet_subnet_gateway    
  #ORG Name
  parent_org_name = var.parent_org_name  
}

