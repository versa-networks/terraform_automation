# Configure the Microsoft Azure Provider
provider "azurerm" {
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "versa_rg" {
  name     = var.resource_group
  location = var.location

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Route Table
resource "azurerm_route_table" "versa_udr" {
  name                = "VersaRouteTable"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
}
# Add Route in Route Table
resource "azurerm_route" "versa_route" {
  name                   = "VersaRoute"
  resource_group_name    = azurerm_resource_group.versa_rg.name
  route_table_name       = azurerm_route_table.versa_udr.name
  address_prefix         = "0.0.0.0/0"
  next_hop_type          = "Internet"
}

# Create virtual network
resource "azurerm_virtual_network" "versaNetwork" {
  name                = "Versa_VPC"
  address_space       = [var.vpc_address_space]
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management Subnet
resource "azurerm_subnet" "mgmt_subnet" {
  name                 = "MGMT-NET"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.mgmt_subnet]
}

# Create south_bound for Director, ServiceVNF and Analytics
resource "azurerm_subnet" "south_bound_network_subnet" {
  name                 = "south_bound_Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.south_bound_network_subnet]
}

# Create Traffic Subnet for controller_1 to ServiceVNF
resource "azurerm_subnet" "ctrl_1_network_subnet" {
  name                 = "Control_1_Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.ctrl_1_network_subnet]
}
# Create Traffic Subnet for controller_2 to ServiceVNF
resource "azurerm_subnet" "ctrl_2_network_subnet" {
  name                 = "Control_2_Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.ctrl_2_network_subnet]
}

# Create WAN transport Traffic Subnet for Controller and Branch
resource "azurerm_subnet" "wan_network_subnet" {
  name                 = "WAN-Network"
  resource_group_name  = azurerm_resource_group.versa_rg.name
  virtual_network_name = azurerm_virtual_network.versaNetwork.name
  address_prefixes     = [var.internet_subnet]
}

# Associate Route Table to Subnet
resource "azurerm_subnet_route_table_association" "subnet_rt_table" {
  subnet_id      = azurerm_subnet.south_bound_network_subnet.id
  route_table_id = azurerm_route_table.versa_udr.id
}
resource "azurerm_subnet_route_table_association" "mgnt_subnet_rt_table" {
  subnet_id      = azurerm_subnet.mgmt_subnet.id
  route_table_id = azurerm_route_table.versa_udr.id
}
# Create Public IP for master Director
resource "azurerm_public_ip" "master_director_Public_IP" {
  count               = length(var.hostname_director)
  name                = "master_director_Public_IP"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Create Public IP for master Director
resource "azurerm_public_ip" "slave_director_Public_IP" {
  count               = length(var.hostname_director)
  name                = "slave_director_Public_IP"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Create Public IP for Controller_1 Internet Interface
resource "azurerm_public_ip" "ip_ctrl_1_inet" {
  name                = "PublicIP_controller_1_INET"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Create Public IP for Controller_2 Internet Interface
resource "azurerm_public_ip" "ip_ctrl_2_inet" {
  name                = "PublicIP_controller_2_INET"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  sku                 = "Standard"
  allocation_method   = "Static"
  availability_zone = var.availability_zone

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Public IP for Controller WAN Interface
# resource "azurerm_public_ip" "ip_ctrl_wan" {
#   count               = length(var.hostname_director)
#   name                = "PublicIP_controller_1_${1 + count.index}_WAN"
#   location            = var.location
#   resource_group_name = azurerm_resource_group.versa_rg.name
#   sku                 = "Standard"
#   allocation_method   = "Static"
#   availability_zone = var.availability_zone

#   tags = {
#     environment = "${var.tag_name}_VersaHeadEnd"
#   }
# }

# # Create Public IP for Analytics
# resource "azurerm_public_ip" "ip_van" {
#   count               = length(var.hostname_van)
#   name                = "PublicIP_VAN_${1 + count.index}"
#   location            = var.location
#   resource_group_name = azurerm_resource_group.versa_rg.name
#   sku                 = "Standard"
#   allocation_method   = "Static"
#   availability_zone   = var.availability_zone

#   tags = {
#     environment = "${var.tag_name}_VersaHeadEnd"
#   }
# }

# Create Network Security Groups and rules for Director
resource "azurerm_network_security_group" "versa_nsg_dir" {
  name                = "VersaDir-NSG"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  security_rule {
    name                       = "Versa_Security_Rule_TCP"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "4566", "4570", "5432", "443", "9182-9183", "2022", "4949", "20514", "6080", "9090"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_UDP"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["20514"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_Outbound"
    priority                   = 251
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Network Security Groups and rules for FlexVNF
resource "azurerm_network_security_group" "versa_nsg_vnf" {
  name                = "VersaFlexVNF-NSG"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  security_rule {
    name                       = "Versa_Security_Rule_TCP"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "2022", "1024-1120", "3000-3003", "9878", "8443", "5201"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_UDP"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["500", "3002-3003", "4500", "4790"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_Outbound"
    priority                   = 251
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_ESP"
    priority                   = 301
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_address_prefix      = "VirtualNetwork"
    destination_address_prefix = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Network Security Groups and rules for Analytics
resource "azurerm_network_security_group" "versa_nsg_van" {
  name                = "VersaAnalytics-NSG"
  location            = var.location
  resource_group_name = azurerm_resource_group.versa_rg.name
  security_rule {
    name                       = "Versa_Security_Rule_TCP"
    priority                   = 151
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = ["22", "443", "2181", "2888", "3888", "7000-7001", "7199", "9042", "9160", "8080", "8443", "1234", "8010", "8020", "5000", "5010", "8008", "8983"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_UDP"
    priority                   = 201
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_ranges    = ["53", "1234", "123"]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Versa_Security_Rule_Outbound"
    priority                   = 251
    direction                  = "Outbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for master_Director
resource "azurerm_network_interface" "master_director_nic_1" {
  name                 = "Master_Director_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Master_Director_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.dir_mgnt_interfaces_IP[0]
    public_ip_address_id          = azurerm_public_ip.master_director_Public_IP[0].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Create Southbound network interface for master_Director
resource "azurerm_network_interface" "master_director_nic_2" {
  name                 = "Master_Director_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Master_Director_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.dir_south_bound_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Create Management network interface for slave_director
resource "azurerm_network_interface" "slave_director_nic_1" {
  name                 = "Slave_Director_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Slave_Director_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.dir_mgnt_interfaces_IP[1]
    public_ip_address_id          = azurerm_public_ip.slave_director_Public_IP[0].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Create Southbound network interface for slave_Director
resource "azurerm_network_interface" "slave_director_nic_2" {
  name                 = "Slave_Director_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Slave_Director_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.dir_south_bound_interfaces_IP[1]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for Controller_1
resource "azurerm_network_interface" "controller_1_nic_1" {
  name                 = "controller_1_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "controller_1_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_flex_mgnt_interfaces_IP[1]
    # public_ip_address_id          = azurerm_public_ip.ip_ctrl[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Northbound/Control network interface for Controller_1
resource "azurerm_network_interface" "controller_1_nic_2" {
  name                 = "controller_1_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "controller_1_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.ctrl_1_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_1_control_network_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/INTERNET network interface for Controller_1
resource "azurerm_network_interface" "controller_1_nic_3" {
  name                 = "controller_1_NIC3"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "controller_1_NIC3_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_internet_interfaces_IP[0]
    public_ip_address_id          = azurerm_public_ip.ip_ctrl_1_inet.id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/WAN network interface for Controller_1
resource "azurerm_network_interface" "controller_1_nic_4" {
  name                 = "controller_1_NIC4"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "controller_1_NIC4_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "dynamic"
    # public_ip_address_id          = azurerm_public_ip.ip_ctrl_wan[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Create Management network interface for Controller_2
resource "azurerm_network_interface" "controller_2_nic_1" {
  name                 = "Controller_2_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Controller_2_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_flex_mgnt_interfaces_IP[2]
    # public_ip_address_id          = azurerm_public_ip.ip_ctrl[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Northbound/Control network interface for Controller_2
resource "azurerm_network_interface" "controller_2_nic_2" {
  name                 = "Controller_2_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Controller_2_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.ctrl_2_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_2_control_network_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/INTERNET network interface for Controller_2
resource "azurerm_network_interface" "controller_2_nic_3" {
  name                 = "Controller_2_NIC3"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Controller_2_NIC3_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_internet_interfaces_IP[1]
    public_ip_address_id          = azurerm_public_ip.ip_ctrl_2_inet.id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound/WAN network interface for Controller_2
resource "azurerm_network_interface" "controller_2_nic_4" {
  name                 = "Controller_2_NIC4"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Controller_2_NIC4_Configuration"
    subnet_id                     = azurerm_subnet.wan_network_subnet.id
    private_ip_address_allocation = "dynamic"
    # public_ip_address_id          = azurerm_public_ip.ip_ctrl_2_wan[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Management network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_1" {
  name                 = "Svnf_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Svnf_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.controller_flex_mgnt_interfaces_IP[0]
    # public_ip_address_id          = azurerm_public_ip.ip_svnf[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_2" {
  name                 = "Svnf_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Svnf_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.service_vnf_south_bound_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_3" {
  name                 = "Svnf_NIC3"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Svnf_NIC3_Configuration"
    subnet_id                     = azurerm_subnet.ctrl_1_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.service_vnf_control_network_1_interfaces_IP[0]
    # public_ip_address_id          = azurerm_public_ip.ip_svnf_inet[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Control network interface for ServiceVNF
resource "azurerm_network_interface" "svnf_nic_4" {
  name                 = "Svnf_NIC4"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "Svnf_NIC4_Configuration"
    subnet_id                     = azurerm_subnet.ctrl_2_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.service_vnf_control_network_2_interfaces_IP[0]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Create Management network interface for Analytics
resource "azurerm_network_interface" "van_nic_1" {
  count                = length(var.hostname_van)
  name                 = "VAN_${1 + count.index}_NIC1"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "VAN_${1 + count.index}_NIC1_Configuration"
    subnet_id                     = azurerm_subnet.mgmt_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ana_mgnt_interfaces_IP[count.index]
    # public_ip_address_id          = azurerm_public_ip.ip_van[count.index].id
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create Southbound network interface for Analytics
resource "azurerm_network_interface" "van_nic_2" {
  count                = length(var.hostname_van)
  name                 = "VAN_${1 + count.index}_NIC2"
  location             = var.location
  resource_group_name  = azurerm_resource_group.versa_rg.name
  enable_ip_forwarding = "true"

  ip_configuration {
    name                          = "VAN_${1 + count.index}_NIC2_Configuration"
    subnet_id                     = azurerm_subnet.south_bound_network_subnet.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ana_south_bound_interfaces_IP[count.index]
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Associate security group to Master Director Management Network Interface
resource "azurerm_network_interface_security_group_association" "master_dir_mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.master_director_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}

# Associate security group to Master Director Southbound Network Interface
resource "azurerm_network_interface_security_group_association" "master_dir_sb_nic_nsg" {
  network_interface_id      = azurerm_network_interface.master_director_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}
# Associate security group to Slave Director Management Network Interface
resource "azurerm_network_interface_security_group_association" "slave_dir_mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.slave_director_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}

# Associate security group to Slave Director Southbound Network Interface
resource "azurerm_network_interface_security_group_association" "slave_dir_sb_nic_nsg" {
  network_interface_id      = azurerm_network_interface.slave_director_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_dir.id
}
# Associate security group to Controller_1 Management Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_1_mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_1_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_1 Control Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_1_nb_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_1_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_1 Internet Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_1_inet_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_1_nic_3.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_1 WAN Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_1_wan_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_1_nic_4.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}
# Associate security group to Controller_2 Management Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_2_mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_2_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_2 Control Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_2_nb_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_2_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_2 Internet Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_2_inet_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_2_nic_3.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Controller_2 WAN Network Interface
resource "azurerm_network_interface_security_group_association" "ctrl_2_wan_nic_nsg" {
  network_interface_id      = azurerm_network_interface.controller_2_nic_4.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}
# Associate security group to SVNF Management Network Interface
resource "azurerm_network_interface_security_group_association" "svnf_mgmt_nic_nsg" {
  network_interface_id      = azurerm_network_interface.svnf_nic_1.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to SVNF Control Network Interface
resource "azurerm_network_interface_security_group_association" "svnf_nb_nic_nsg" {
  network_interface_id      = azurerm_network_interface.svnf_nic_2.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to SVNF Connects controller
resource "azurerm_network_interface_security_group_association" "svnf_control_nic_nsg" {
  network_interface_id      = azurerm_network_interface.svnf_nic_4.id
  network_security_group_id = azurerm_network_security_group.versa_nsg_vnf.id
}

# Associate security group to Analytics Management Network Interface
resource "azurerm_network_interface_security_group_association" "van_mgmt_nic_nsg" {
  count                     = length(var.hostname_van)
  network_interface_id      = azurerm_network_interface.van_nic_1[count.index].id
  network_security_group_id = azurerm_network_security_group.versa_nsg_van.id
}

# Associate security group to Analytics Southbound Network Interface
resource "azurerm_network_interface_security_group_association" "van_sb_nic_nsg" {
  count                     = length(var.hostname_van)
  network_interface_id      = azurerm_network_interface.van_nic_2[count.index].id
  network_security_group_id = azurerm_network_security_group.versa_nsg_van.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    resource_group = azurerm_resource_group.versa_rg.name
  }

  byte_length = 4
}

# Create storage account for boot diagnostics of Director VM
resource "azurerm_storage_account" "storageaccountDir" {
  count                    = length(var.hostname_director)
  name                     = "dir${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create storage account for boot diagnostics of Controller VM
resource "azurerm_storage_account" "storageaccountCtrl" {
  count                    = length(var.hostname_director)
  name                     = "ctrl${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create storage account for boot diagnostics of ServiceVNF VM
resource "azurerm_storage_account" "storageaccountSVNF" {
  count                    = length(var.hostname_director)
  name                     = "svnf${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Create storage account for boot diagnostics of Analytics VM
resource "azurerm_storage_account" "storageaccountVAN" {
  count                    = length(var.hostname_van)
  name                     = "van${1 + count.index}diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.versa_rg.name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Add template to use custom data for Director:
data "template_file" "user_data_master_dir" {
  template = file("./master_director.sh")
  vars = {
    sshkey              = var.ssh_key
    parent_org_name = var.parent_org_name
    hostname_dir_1 = var.Master_director_Hostname[0]
    hostname_dir_2 = var.Slave_director_Hostname[0]
    analytics_1_hostname = var.hostname_van[0]
    search_1_hostname = var.hostname_van[1]
    mgmt_subnet_gateway = var.mgmt_subnet_gateway    
    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    master_dir_south_bound_ip = azurerm_network_interface.master_director_nic_2.private_ip_address
    slave_dir_mgmt_ip = azurerm_network_interface.slave_director_nic_1.private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.slave_director_nic_2.private_ip_address
    DC_router_eth0_mgnt_ip = azurerm_network_interface.svnf_nic_1.private_ip_address
    dc_router_mgnt_ip = azurerm_network_interface.svnf_nic_4.private_ip_address
    analytics_mgnt_ip = azurerm_network_interface.van_nic_1[0].private_ip_address
    search_mgnt_ip = azurerm_network_interface.van_nic_1[1].private_ip_address
    analytics_south_bound_ip = azurerm_network_interface.van_nic_2[0].private_ip_address
    search_south_bound_ip = azurerm_network_interface.van_nic_2[1].private_ip_address
    overlay_prefixes = var.overlay_prefixes    
    router_south_bound_ip = azurerm_network_interface.svnf_nic_2.private_ip_address 

    controller_1_hostname = var.controller_1_hostname
    controller_1_country = var.controller_1_country
    controller_1_mgnt_ip = azurerm_network_interface.controller_1_nic_1.private_ip_address
    controller_1_south_bound_ip = azurerm_network_interface.controller_1_nic_2.private_ip_address
    controller_1_internet_private_ip = azurerm_network_interface.controller_1_nic_3.private_ip_address
    controller_1_internet_public_ip = azurerm_public_ip.ip_ctrl_1_inet.ip_address
    controller_1_south_bound_subnet = azurerm_subnet.ctrl_1_network_subnet.address_prefix[0]
    controller_1_internet_subnet = azurerm_subnet.wan_network_subnet.address_prefix[0]
    controller_1_internet_subnet_gateway = var.internet_subnet_gateway
    controller_1_router_south_bound_ip = azurerm_network_interface.svnf_nic_3.private_ip_address
    
    controller_2_hostname = var.controller_2_hostname
    controller_2_country = var.controller_2_country
    controller_2_mgnt_ip = azurerm_network_interface.controller_2_nic_1.private_ip_address
    controller_2_south_bound_ip = azurerm_network_interface.controller_2_nic_2.private_ip_address
    controller_2_internet_private_ip = azurerm_network_interface.controller_2_nic_3.private_ip_address
    controller_2_internet_public_ip = azurerm_public_ip.ip_ctrl_2_inet.ip_address
    controller_2_south_bound_subnet = azurerm_subnet.ctrl_2_network_subnet.address_prefix[0]
    controller_2_internet_subnet = azurerm_subnet.wan_network_subnet.address_prefix[0]
    controller_2_internet_subnet_gateway = var.internet_subnet_gateway
    controller_2_router_south_bound_ip = azurerm_network_interface.svnf_nic_4.private_ip_address
  }
}

# Create Versa Director Virtual Machine
resource "azurerm_virtual_machine" "master_director" {
  count                        = length(var.hostname_director)
  name                         = "Versa_Director_${1 + count.index}"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.master_dir_mgmt_nic_nsg, azurerm_network_interface_security_group_association.master_dir_sb_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.master_director_nic_1.id, azurerm_network_interface.master_director_nic_2.id]
  primary_network_interface_id = azurerm_network_interface.master_director_nic_1.id
  vm_size                      = var.director_vm_size

  storage_os_disk {
    name              = "Master_Director_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_director
  }

  os_profile {
    computer_name  = var.Master_director_Hostname[0]
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_master_dir.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountDir[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Add template to use custom data for Director:
data "template_file" "user_data_slave_director" {
  template = file("./slave_director.sh")

  vars = {
    sshkey              = var.ssh_key
    hostname_dir_1 = var.Master_director_Hostname[0]
    hostname_dir_2 = var.Slave_director_Hostname[0]
    mgmt_subnet_gateway = var.mgmt_subnet_gateway    
    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    slave_dir_mgmt_ip = azurerm_network_interface.slave_director_nic_1.private_ip_address
    analytics_mgnt_ip = azurerm_network_interface.van_nic_1[0].private_ip_address
    search_mgnt_ip = azurerm_network_interface.van_nic_1[1].private_ip_address    

  }
}

# Create Versa Director Virtual Machine
resource "azurerm_virtual_machine" "slave_director" {
  count                        = length(var.hostname_director)
  name                         = "slave_director"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.slave_dir_mgmt_nic_nsg, azurerm_network_interface_security_group_association.slave_dir_sb_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.slave_director_nic_1.id, azurerm_network_interface.slave_director_nic_2.id]
  primary_network_interface_id = azurerm_network_interface.slave_director_nic_1.id
  vm_size                      = var.director_vm_size

  storage_os_disk {
    name              = "Slave_Director_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_director
  }

  os_profile {
    computer_name  = var.Slave_director_Hostname[0]
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_slave_director.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountDir[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Add template to use custom data for Analytics:
data "template_file" "user_data_van" {
  count    = length(var.hostname_van)
  template = file("./van.sh")

  vars = {
    sshkey              = var.ssh_key
    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    slave_dir_mgmt_ip  = azurerm_network_interface.slave_director_nic_1.private_ip_address
    ctrl_1_network_subnet = azurerm_subnet.ctrl_1_network_subnet.address_prefix
    ctrl_2_network_subnet = azurerm_subnet.ctrl_2_network_subnet.address_prefix
    router_mgnt_ip = azurerm_network_interface.svnf_nic_2.private_ip_address
  }
}
# Create Versa Analytics Virtual Machine
resource "azurerm_virtual_machine" "vanVM" {
  count                        = length(var.hostname_van)
  name                         = "Versa_Analytics_${1 + count.index}"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.van_mgmt_nic_nsg, azurerm_network_interface_security_group_association.van_sb_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.van_nic_1[count.index].id, azurerm_network_interface.van_nic_2[count.index].id]
  primary_network_interface_id = azurerm_network_interface.van_nic_1[count.index].id
  vm_size                      = var.analytics_vm_size

  storage_os_disk {
    name              = "VAN_${1 + count.index}_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
    disk_size_gb      = var.van_disk_size
  }

  storage_image_reference {
    id = var.image_analytics
  }

  os_profile {
    computer_name  = var.hostname_van[count.index]
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_van[count.index].rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountVAN[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}


# Add template to use custom data for Controller_1:
data "template_file" "user_data_controller_1" {
  # count    = length(var.hostname_director)
  template = file("./controller.sh")

  vars = {
    sshkey             = var.ssh_key
    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    master_dir_south_bound_ip = azurerm_network_interface.master_director_nic_2.private_ip_address
    slave_dir_mgmt_ip = azurerm_network_interface.slave_director_nic_1.private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.slave_director_nic_2.private_ip_address
  }
}

# Create Versa Controller Virtual Machine
resource "azurerm_virtual_machine" "controller_1" {
  count                        = length(var.hostname_director)
  name                         = "Versa_controller_1"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.ctrl_1_mgmt_nic_nsg, azurerm_network_interface_security_group_association.ctrl_1_nb_nic_nsg, azurerm_network_interface_security_group_association.ctrl_1_inet_nic_nsg, azurerm_network_interface_security_group_association.ctrl_1_wan_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.controller_1_nic_1.id, azurerm_network_interface.controller_1_nic_2.id, azurerm_network_interface.controller_1_nic_3.id, azurerm_network_interface.controller_1_nic_4.id]
  primary_network_interface_id = azurerm_network_interface.controller_1_nic_1.id
  vm_size                      = var.controller_vm_size

  storage_os_disk {
    name              = "controller_1_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_controller
  }

  os_profile {
    computer_name  = "versa-flexvnf"
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_controller_1.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountCtrl[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}

# Add template to use custom data for Controller_2:
data "template_file" "user_data_Controller_2" {
  # count    = length(var.hostname_director)
  template = file("./controller.sh")

  vars = {
    sshkey             = var.ssh_key
    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    master_dir_south_bound_ip = azurerm_network_interface.master_director_nic_2.private_ip_address
    slave_dir_mgmt_ip = azurerm_network_interface.slave_director_nic_1.private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.slave_director_nic_2.private_ip_address
  }
}

# Create Versa Controller Virtual Machine
resource "azurerm_virtual_machine" "controller_2" {
  count                        = length(var.hostname_director)
  name                         = "Versa_Controller_2"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.ctrl_2_mgmt_nic_nsg, azurerm_network_interface_security_group_association.ctrl_2_nb_nic_nsg, azurerm_network_interface_security_group_association.ctrl_2_inet_nic_nsg, azurerm_network_interface_security_group_association.ctrl_2_wan_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.controller_2_nic_1.id, azurerm_network_interface.controller_2_nic_2.id, azurerm_network_interface.controller_2_nic_3.id, azurerm_network_interface.controller_2_nic_4.id]
  primary_network_interface_id = azurerm_network_interface.controller_2_nic_1.id
  vm_size                      = var.controller_vm_size

  storage_os_disk {
    name              = "Controller_2_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_controller
  }

  os_profile {
    computer_name  = "versa-flexvnf"
    admin_username = "versa_devops"
    custom_data    = data.template_file.user_data_Controller_2.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountCtrl[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
# Add template to use custom data for ServiceVNF:
data "template_file" "user_data_svnf" {
  # count    = length(var.hostname_director)
  template = file("./dc_dr_network_config_gen.yaml")

  vars = {
    sshkey             = var.ssh_key
    parent_org_name = var.parent_org_name    
    South_Bound_Network_ip = azurerm_network_interface.svnf_nic_2.private_ip_address
    Controller_1_connect  = azurerm_network_interface.svnf_nic_3.private_ip_address
    Controller_2_connect  = azurerm_network_interface.svnf_nic_4.private_ip_address

    master_dir_mgmt_ip  = azurerm_network_interface.master_director_nic_1.private_ip_address
    master_dir_south_bound_ip = azurerm_network_interface.master_director_nic_2.private_ip_address
    slave_dir_mgmt_ip = azurerm_network_interface.slave_director_nic_1.private_ip_address
    slave_dir_south_bound_ip = azurerm_network_interface.slave_director_nic_2.private_ip_address
    
    controller_1_router_ip = azurerm_network_interface.controller_1_nic_2.private_ip_address
    controller_2_router_ip = azurerm_network_interface.controller_2_nic_2.private_ip_address
  }
}

data "template_cloudinit_config" "user_data_dc_router_config" {
  gzip          = true
  base64_encode = true
  part {
    content_type = "text/cloud-config"
    content      = data.template_file.user_data_svnf.rendered
  }
}

# Create Versa SVNF Virtual Machine
resource "azurerm_virtual_machine" "svnfVM" {
  count                        = length(var.hostname_director)
  name                         = "Versa_SVNF"
  location                     = var.location
  zones                        = [var.availability_zone]
  resource_group_name          = azurerm_resource_group.versa_rg.name
  depends_on                   = [azurerm_network_interface_security_group_association.svnf_mgmt_nic_nsg, azurerm_network_interface_security_group_association.svnf_nb_nic_nsg, azurerm_network_interface_security_group_association.svnf_control_nic_nsg]
  network_interface_ids        = [azurerm_network_interface.svnf_nic_1.id, azurerm_network_interface.svnf_nic_2.id, azurerm_network_interface.svnf_nic_3.id, azurerm_network_interface.svnf_nic_4.id]
  primary_network_interface_id = azurerm_network_interface.svnf_nic_1.id
  vm_size                      = var.svnf_vm_size

  storage_os_disk {
    name              = "Svnf_${1 + count.index}_OSDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    id = var.image_svnf
  }

  os_profile {
    computer_name  = "versa-flexvnf"
    admin_username = "versa_devops"
    custom_data    = data.template_cloudinit_config.user_data_dc_router_config.rendered
  }

  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/versa_devops/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }

  boot_diagnostics {
    enabled     = "true"
    storage_uri = azurerm_storage_account.storageaccountSVNF[count.index].primary_blob_endpoint
  }

  tags = {
    environment = "${var.tag_name}_VersaHeadEnd"
  }
}
