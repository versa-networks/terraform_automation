# #common variables
subscription_id =  "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_secret = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tenant_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tag_name = "versa_se_tf"
ssh_key =  "ssh-rsa xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
private_key_name = "dkumar"
director_vm_size =  "Standard_B2ms"
controller_vm_size =  "Standard_B4ms"
svnf_vm_size =  "Standard_B4ms"
analytics_vm_size =  "Standard_B4ms"
van_disk_size =  "80"

#Host_Name_Info

Master_director_Hostname =  ["versa-director-1"]
Slave_director_Hostname =  ["versa-director-2"]
hostname_van =  ["versa-analytics-1", "versa-analytics-2"]
controller_1_hostname = "Controller-1"
controller_1_country = "Singapore"
controller_2_hostname = "Controller-2"
controller_2_country = "california"

#Image_Details
#DC_image_details
# 21.2.1
# image_director =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_VD_21.2_18.04"
# image_controller =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_VOS_21.2_14.04"
# image_svnf =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_VOS_21.2_14.04"
# image_analytics = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_VAN_21.2_18.04"

# 21.3.1
image_director =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VD_21_3_1_Bionic"
image_controller =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VNF_21_3_1_Trusty"
image_svnf =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VNF_21_3_1_Trusty"
image_analytics = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VOS_21_3_1_Bionic"


#DC variables
location =  "eastus2"
availability_zone =  "2"
resource_group =  "dkumar_tf"
vpc_address_space =  "10.153.0.0/16"
newbits_subnet =  "8"
#### DC MGNT Subnet INFO ####
mgmt_subnet = "10.153.0.0/24"
mgmt_subnet_gateway = "10.153.0.1"
#["Master_Director",Slave_Director]
dir_mgnt_interfaces_IP = ["10.153.0.21","10.153.0.22"]
# ["Analytics","Search"]
ana_mgnt_interfaces_IP = ["10.153.0.26","10.153.0.27"]
#[service_vnf,Controller1,Controller2]
controller_flex_mgnt_interfaces_IP = ["10.153.0.23","10.153.0.24","10.153.0.25"]
#### SOUTH_BOUND Subnet INFO ####
south_bound_network_subnet = "10.153.1.0/24"
#["Master_Director",Slave_Director]
dir_south_bound_interfaces_IP = ["10.153.1.21","10.153.1.22"]
#["Analytics","Search"]
ana_south_bound_interfaces_IP = ["10.153.1.26","10.153.1.27"]
#[service_vnf]
service_vnf_south_bound_interfaces_IP = ["10.153.1.23"]
#### ctrl_1 Subnet INFO ####
ctrl_1_network_subnet = "10.153.2.0/24"
service_vnf_control_network_1_interfaces_IP = ["10.153.2.23"]
controller_1_control_network_interfaces_IP = ["10.153.2.24"]
#### ctrl_2 Subnet INFO ####
ctrl_2_network_subnet = "10.153.3.0/24"
service_vnf_control_network_2_interfaces_IP = ["10.153.3.23"]
controller_2_control_network_interfaces_IP = ["10.153.3.25"]
#### DC INTERNET Subnet INFO ####
internet_subnet = "10.153.4.0/24"
internet_subnet_gateway = "10.153.4.1"
# [Controller1,Controller2]
controller_internet_interfaces_IP = ["10.153.4.24","10.153.4.25"]
#ORG NAME
parent_org_name = "versa"
overlay_prefixes = "10.0.0.0/8"

