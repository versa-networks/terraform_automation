output "Master_Director_Instance" {
  value = azurerm_virtual_machine.master_director[0].name
}
output "Master_Director_MGMT_IP" {
  value = azurerm_network_interface.master_director_nic_1.private_ip_address
}

output "Master_Director_Public_IP" {
  value = azurerm_public_ip.master_director_Public_IP[0].ip_address
}

output "Master_Director_CLI" {
  value = "ssh -i ${var.private_key_name}.pem Administrator@${azurerm_public_ip.master_director_Public_IP[0].ip_address}"
}

output "Master_Director_UI_Login" {
  value = "https://${azurerm_public_ip.master_director_Public_IP[0].ip_address}"
}
output "Slave_Director_Instance" {
  value = azurerm_virtual_machine.slave_director[0].name
}

output "Slave_Director_MGMT_IP" {
  value = azurerm_network_interface.slave_director_nic_1.private_ip_address
}

output "Slave_Director_Public_IP" {
  value = azurerm_public_ip.slave_director_Public_IP[0].ip_address
}

output "Slave_Director_CLI" {
  value = "ssh -i id_rsa Administrator@${azurerm_public_ip.slave_director_Public_IP[0].ip_address}"
}

output "Slave_Director_UI_Login" {
  value = "https://${azurerm_public_ip.slave_director_Public_IP[0].ip_address}"
}



output "Versa_Analytics-1_Instance" {
  value = azurerm_virtual_machine.vanVM[0].name
}

output "Versa_Analytics-1_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[0].private_ip_address
}

# output "Versa_Analytics-1_Instance_Public_IP" {
#   value = azurerm_public_ip.ip_van[0].ip_address
# }

output "Versa_Analytics-2_Instance" {
  value = azurerm_virtual_machine.vanVM[1].name
}

output "Versa_Analytics-2_Instance_MGMT_IP" {
  value = azurerm_network_interface.van_nic_1[1].private_ip_address
}

# output "Versa_Analytics-2_Instance_Public_IP" {
#   value = azurerm_public_ip.ip_van[1].ip_address
# }


output "Versa_Controller-1_Instance" {
  value = azurerm_virtual_machine.controller_1[0].name
}

output "Versa_Controller-1_MGMT_IP" {
  value = azurerm_network_interface.controller_1_nic_1.private_ip_address
}

output "Versa_Controller-1_InternetTransport_Public_IP" {
  value = azurerm_public_ip.ip_ctrl_1_inet.ip_address
}

output "Versa_Controller-2_Instance" {
  value = azurerm_virtual_machine.controller_2[0].name
}

output "Versa_Controller-2_MGMT_IP" {
  value = azurerm_network_interface.controller_2_nic_1.private_ip_address
}

output "Versa_Controller-2_InternetTransport_Public_IP" {
  value = azurerm_public_ip.ip_ctrl_2_inet.ip_address
}

