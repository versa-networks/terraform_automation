This Terraform Template is intended to Automate and bringing up Versa Head End setup in one touch mode.
It will bring up 2 instances of Versa Director, 2 analytics node, service VNF & 2 Versa Controller's in single regions.

# Pre-requisites for using this template:

- **Terraform Install:** To Download & Install Terraform, refer Link "www.terraform.io/downloads.html"
- **Versa Head End Images:** Image available should available in region:
  - Versa Director
  - Versa Controller/service VNF
  - Versa Analytics


# Usage:

- Download all the files in PC where Terraform is installed. It is recommended that place all the files in folder as terraform will store the state file for this environment once it is applied.
- Go to the folder "versa_poc_he_auto_config_v1" where all the required files are placed.

- Use command `terraform init` to initialize. it will download necessary terraform plugins required to run this template.
- Then use command `terraform plan` to plan the deployment. It will show the plan regarding all the resources being provisioned as part of this template.
- At last use command `terraform apply` to apply this plan in action for deployment. It will start deploying all the resource on Azure.

The following figure illustrates the redundant (high availability) headend topology created by the Terraform template(s).

It will require below files to run it.


.
|____output.tf
|____van.sh
|____main.tf
|____terraform.tfvars
|____dc_dr_network_config_gen.yaml
|____README.md
|____slave_director.sh
|____controller.sh
|____master_director.sh
|____vars.tf


**main.tf file:**

main.tf template file will perform below actions/activities when executed:

- It will deploy (Master_Director,slave_director,Analytics,Search,service VNF, controller-1 and controller-2) in region.
- once instances deployed, Master_Director and Slave_Director will initialize the vnms-startup script in non-interactive mode
-service VNF will initialize the cloud-init script along with BGP configuration,
- Master_Director and Slave_Director will form the HA,
- once HA is ready, then van_cluster_installer.py will start to integrate the analytics.
- Post van_cluster_installer.py service VNF, controller-1 and Controller-2 will added to the Director.


**var.tf file:**

var.tf file will have definition for all the variables defined/used as part of this template. User does not need to update anything in this file.

**terraform.tfvars file:**

terraform.tfvars file is being used to get the variables values from user. User need to update the required fields according to their environment. This file will have below information:

###### common variables ######
- subscription_id : Provide the Subscription ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- client_id : Provide the Client ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- client_secret : Provide the Client Secret information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- tenant_id : Provide the Tenant ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.

###### file path example /Users/dinz/Downloads ######
private_key_name : Provide the key pair name Eg: "customer_name"
ssh_key : Provide the ssh public key information here. This key will be injected into instances which can be used to login into instances later on. Azure does not provide the option to generate the keys. User has to generate the ssh key using "sshkey-gen" or "putty key generator" tool to generate the ssh keys. Here Public key information is required. Eg: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bi1jnbgPOVHq2d1Vu1lpc+i3HfRlCxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

###### Host Name ######

dc_hostname_director : Provide the hostname for master Director instances
dr_hostname_director : Provide the hostname for slave Director instances
hostname_van : Provide the hostname for Analytics and search nodes Eg:["versa-analytics-1", "versa-analytics-2"]
controller_1_hostname : Provide the hostname for controller-1 instances
controller_1_country : Provide the country info for controller-1 instances
controller_2_hostname : Provide the hostname for controller-2 instances
controller_2_country : Provide the country info for controller-2 instances

###### INSTANCE TYPE ######

director_vm_size : Provide the instance type which will be used to provision the Versa Director Instance. By default, standard_F8s_v2 will be used.
analytics_vm_size : Provide the instance type which will be used to provision the Versa Analytics Instance. By default, standard_F8s_v2 will be used.
controller_vm_size : Provide the instance type which will be used to provision the Versa Controller Instance. By default, standard_F8s_v2 will be used.
svnf_vm_size : Provide the instance type which will be used to provision the Versa service VNF Instance. By default, standard_F8s_v2 will be used.

###### IMAGE'S ######

image_director : Provide the Versa Director image for region
image_analytics : Provide the Versa Analytics image for region
image_controller/dc_image_svnf : Provide the Versa VOS image for region

###### CIDR Subnet INFO ######

cidr_block : Provide the subnet info. By default "10.153.0.0/16" will be created in .

###### MGNT Subnet INFO ######

mgmt_subnet : Provide the  management/North_bound subnet info. By default "10.153.0.0/24" will be created as management/North_bound subnet in .
mgmt_subnet_gateway : Provide the  management/North_bound gateway IP info. By default "10.153.0.1" will be created as management/North_bound gateway in .
dir_mgnt_interfaces_IP : Provide the management IP details for ["Master_Director",""Slave_Director"]. By default ["10.153.0.21","10.153.0.22"] will be created respectively.
ana_mgnt_interfaces_IP : Provide the management IP details for ["Analytics","Search"]. By default ["10.153.0.26","10.153.0.27"] will be created respectively.
controller_flex_mgnt_interfaces_IP : Provide the management IP details for [service_vnf,Controller1,Controller2]. By default ["10.153.0.23","10.153.0.24","10.153.0.25"] will be created respectively.


#### SOUTH_BOUND Subnet INFO ####

south_bound_network_subnet : Provide the  south bound subnet info. By default "10.153.1.0/24" will be created as south bound subnet in .
dir_south_bound_interfaces_IP : Provide the south bound IP details for ["Master_Director",Slave_Director]. By default ["10.153.1.21","10.153.1.22"] will be created respectively.
ana_south_bound_interfaces_IP : Provide the south bound IP details for ["Analytics","Search"]. By default ["10.153.1.26","10.153.1.27"] will be created respectively.
service_vnf_south_bound_interfaces_IP : Provide the south bound IP details for ["service_vnf"]. By default ["10.153.1.237"] will be created respectively.

#### Control_1 Network Subnet INFO ####

ctrl_1_network_subnet : Provide the  Control_1 Network subnet info. By default "10.153.2.0/24" will be created.
service_vnf_control_network_1_interfaces_IP : Provide the south bound IP details for [service_vnf]. By default ["10.153.2.23"] will be created respectively.
controller_1_control_network_interfaces_IP : Provide the south bound IP details for [service_vnf,Controller1]. By default ["10.153.2.24"] will be created respectively.

#### Control_2 Network Subnet INFO ####

ctrl_2_network_subnet : Provide the  Control_1 Network subnet info. By default "10.153.3.0/24" will be created.
service_vnf_control_network_2_interfaces_IP : Provide the south bound IP details for [service_vnf]. By default ["10.153.3.23"] will be created respectively.
controller_2_control_network_interfaces_IP : Provide the south bound IP details for [service_vnf,Controller1]. By default ["10.153.3.25"] will be created respectively.


#### INTERNET Subnet INFO ####

internet_subnet : Provide the  internet subnet info. By default "10.153.4.0/24" will be created as internet subnet in .
internet_subnet_gateway : Provide the  internet gateway IP info. By default "10.153.4.1" will be created as internet gateway in .
controller_internet_interfaces_IP : Provide the  internet IP details for [Controller1,Controller2]. By default ["10.153.4.24","10.153.4.25"] will be created respectively.


#### ORG NAME ####
parent_org_name = Provide the parent ORG name. By default "versa" will be used as parent ORG
overlay_prefixes = Provide the overlay prefixes . By default "10.0.0.0/8" will be used as overlay prefixes


**output.tf file:**

output.tf file will have information to provide the output

