module "versa_he_1000_cpe_dc" {
  source = "./versa_he_1000_cpe_multiregion_dc"
  subscription_id =  var.subscription_id
  client_id =  var.client_id
  client_secret =  var.client_secret
  tenant_id =  var.tenant_id
  tag_name = var.tag_name
  private_key_name = var.private_key_name  
  location =  var.dc_location
  availability_zone =  var.dc_availability_zone
  resource_group =  var.dc_resource_group
  ssh_key =  var.ssh_key
  vpc_address_space =  var.dc_vpc_address_space
  newbits_subnet =  var.dc_newbits_subnet
  image_director =  var.dc_image_director
  image_controller =  var.dc_image_controller
  image_svnf =  var.dc_image_svnf
  image_analytics =  var.dc_image_analytics
  hostname_director =  var.dc_hostname_director
  hostname_dir_2 = var.dr_hostname_director  
  hostname_dir_1 = var.dc_hostname_director  
  hostname_van =  var.hostname_van
  director_vm_size =  var.director_vm_size
  controller_vm_size =  var.controller_vm_size
  svnf_vm_size =  var.svnf_vm_size
  analytics_vm_size =  var.analytics_vm_size
  dir_disk_size =  var.dir_disk_size
  van_disk_size =  var.van_disk_size
  controller_disk_size =  var.controller_disk_size
  #MGNT Subnet INFO
  mgmt_subnet = var.DC_mgmt_subnet
  mgmt_subnet_gateway = var.DC_mgmt_subnet_gateway
  DR_mgmt_subnet = var.DR_mgmt_subnet
  #SOUTH_BOUND Subnet INFO
  south_bound_network_subnet = var.DC_south_bound_network_subnet
  #CONTROL NETWORK Subnet INFO
  control_network_subnet = var.DC_control_network_subnet
  #INTERNET Subnet INFO
  internet_subnet = var.DC_internet_subnet
  DC_internet_subnet_gateway = var.DC_internet_subnet_gateway
  #INTER NETWORK Subnet INFO
  inter_network_subnet = var.DC_inter_network_subnet
  inter_network_subnet_gateway = var.DC_inter_network_subnet_gateway
  #IP_DETAILS
  DC_dir_mgnt_interfaces_IP = var.DC_dir_mgnt_interfaces_IP
  DC_ana_mgnt_interfaces_IP = var.DC_ana_mgnt_interfaces_IP
  DC_controller_flex_mgnt_interfaces_IP = var.DC_controller_flex_mgnt_interfaces_IP
  DC_south_bound_network_interfaces_IP = var.DC_south_bound_network_interfaces_IP
  DC_ana_south_bound_network_interfaces_IP = var.DC_ana_south_bound_network_interfaces_IP
  DC_control_network_interface_IP = var.DC_control_network_interface_IP
  DC_inter_network_interface_IP = var.DC_inter_network_interface_IP
  DC_internet_subnet_interface_IP = var.DC_internet_subnet_interface_IP  
  #Script INFO
  slave_dir_mgmt_ip = module.versa_he_1000_cpe_dr.Slave_Director_MGMT_IP
  slave_dir_south_bound_ip = module.versa_he_1000_cpe_dr.Slave_Director_south_bound_IP
  DR_internet_public_IP = module.versa_he_1000_cpe_dr.DR_Router_Internet_Public_IP
  DR_internet_private_IP = module.versa_he_1000_cpe_dr.DR_Router_Internet_private_IP
  DR_router_eth0_mgnt_ip = module.versa_he_1000_cpe_dr.DR_Router_MGMT_IP
  DR_south_bound_network_subnet = var.DR_south_bound_network_subnet
  DR_control_network_subnet = var.DR_control_network_subnet
  #ORG Name
  parent_org_name = var.parent_org_name
  overlay_prefixes = var.overlay_prefixes  
  # controller_flex_internet_network_interfaces_IP = var.DC_controller_flex_internet_network_interfaces_IP
  controller_1_hostname = var.controller_1_hostname
  controller_1_country = var.controller_1_country
  controller_2_hostname = var.controller_2_hostname
  controller_2_country = var.controller_2_country

  #Controller_2 config
  controller_2_mgnt_ip = module.versa_he_1000_cpe_dr.Versa_Controller_2_MGMT_IP
  controller_2_south_bound_subent = module.versa_he_1000_cpe_dr.DR_south_bound_network_subnet
  controller_2_internet_subent = module.versa_he_1000_cpe_dr.DR_internet_subnet
  DR_internet_subnet_gateway = var.DR_internet_subnet_gateway
  controller_2_south_bound_ip = module.versa_he_1000_cpe_dr.Versa_Controller_2_south_bound_Private_IP
  controller_2_internet_private_ip = module.versa_he_1000_cpe_dr.Versa_Controller_2_Internet_Private_IP
  controller_2_internet_public_ip = module.versa_he_1000_cpe_dr.Versa_Controller_2_InternetTransport_Public_IP
  dr_router_south_bound_ip = module.versa_he_1000_cpe_dr.DR_Router_south_bound_private_IP  
  dr_router_control_ntw_ip = module.versa_he_1000_cpe_dr.dr_router_control_ntw_ip  
  dc_south_bound_network_gateway = var.dc_south_bound_network_gateway
}
module "versa_he_1000_cpe_dr" {
  source = "./versa_he_1000_cpe_multiregion_dr"
  subscription_id =  var.subscription_id
  client_id =  var.client_id
  client_secret =  var.client_secret
  tenant_id =  var.tenant_id
  tag_name = var.tag_name
  private_key_name = var.private_key_name    
  location =  var.dr_location
  availability_zone =  var.dr_availability_zone
  resource_group =  var.dr_resource_group
  ssh_key =  var.ssh_key
  vpc_address_space =  var.dr_vpc_address_space
  newbits_subnet =  var.dr_newbits_subnet
  image_director =  var.dr_image_director
  image_controller =  var.dr_image_controller
  image_svnf =  var.dr_image_svnf
  hostname_director =  var.dr_hostname_director
  hostname_dir_1 = var.dc_hostname_director
  director_vm_size =  var.director_vm_size
  controller_vm_size =  var.controller_vm_size
  svnf_vm_size =  var.svnf_vm_size
  dir_disk_size =  var.dir_disk_size
  controller_disk_size =  var.controller_disk_size  
  #ORG Name
  parent_org_name = var.parent_org_name
  overlay_prefixes = var.overlay_prefixes    
  #MGNT Subnet INFO
  mgmt_subnet = var.DR_mgmt_subnet
  mgmt_subnet_gateway = var.DR_mgmt_subnet_gateway  
  DC_mgmt_subnet = var.DC_mgmt_subnet
  DC_south_bound_network_subnet = var.DC_south_bound_network_subnet
  #SOUTH_BOUND Subnet INFO  
  south_bound_network_subnet = var.DR_south_bound_network_subnet  
  #CONTROL NETWORK Subnet INFO
  control_network_subnet = var.DR_control_network_subnet  
  #INTER NETWORK Subnet INFO
  inter_network_subnet = var.DR_inter_network_subnet
  inter_network_subnet_gateway = var.DR_inter_network_subnet_gateway  
  #INTERNET Subnet INFO   
  internet_subnet = var.DR_internet_subnet
  #IP_DETAILS
  DR_dir_mgnt_interfaces_IP = var.DR_dir_mgnt_interfaces_IP
  DR_controller_flex_mgnt_interfaces_IP = var.DR_controller_flex_mgnt_interfaces_IP
  DR_south_bound_network_interfaces_IP = var.DR_south_bound_network_interfaces_IP
  DR_control_network_interface_IP = var.DR_control_network_interface_IP
  DR_inter_network_interface_IP = var.DR_inter_network_interface_IP
  DR_internet_subnet_interface_IP = var.DR_internet_subnet_interface_IP    
  #Script INFO
  master_dir_mgmt_ip = module.versa_he_1000_cpe_dc.Master_Director_MGMT_IP
  master_dir_south_bound_ip = module.versa_he_1000_cpe_dc.Master_Director_south_bound_IP
  DC_internet_public_IP = module.versa_he_1000_cpe_dc.DC_Router_Internet_Public_IP
  DC_internet_private_IP = module.versa_he_1000_cpe_dc.DC_Router_Internet_private_IP
  DR_internet_subnet_gateway = var.DR_internet_subnet_gateway  
  dr_south_bound_network_gateway = var.dr_south_bound_network_gateway
  Versa_Analytics_1_Instance_south_bound_IP = module.versa_he_1000_cpe_dc.Versa_Analytics_1_Instance_south_bound_IP
  Versa_Analytics_2_Instance_south_bound_IP = module.versa_he_1000_cpe_dc.Versa_Analytics_2_Instance_south_bound_IP
  Versa_Search_1_Instance_south_bound_IP = module.versa_he_1000_cpe_dc.Versa_Search_1_Instance_south_bound_IP
  Versa_Search_2_Instance_south_bound_IP = module.versa_he_1000_cpe_dc.Versa_Search_2_Instance_south_bound_IP
}