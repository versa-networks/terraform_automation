#common variables
subscription_id =  "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
client_secret = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tenant_id = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
tag_name = "customer_name_tf"
private_key_name = "customer_name"
ssh_key =  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bi1jnbgPOVHq2d1Vu1lpc+i3HfRlCxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

# #production
# director_vm_size =  "standard_F8s_v2"
# controller_vm_size =  "standard_F8s_v2"
# svnf_vm_size =  "standard_F8s_v2"
# analytics_vm_size =  "standard_F8s_v2"
dir_disk_size = "200"
van_disk_size =  "1024"
controller_disk_size = "120"

#LAB
director_vm_size =  "Standard_B2ms"
controller_vm_size =  "Standard_B4ms"
svnf_vm_size =  "Standard_B4ms"
analytics_vm_size =  "Standard_B4ms"

#Host_Name_Info

dc_hostname_director =  ["versa-director-1"]
dr_hostname_director =  ["versa-director-2"]
hostname_van =  ["versa-analytics-1", "versa-analytics-2","versa-search-1","versa-search-2"]
controller_1_hostname = "Controller-1"
controller_1_country = "Singapore"
controller_2_hostname = "Controller-2"
controller_2_country = "california"

#Image_Details
#DC_image_details
dc_image_director =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VD_21_3_1_Bionic"
dc_image_controller =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VNF_21_3_1_Trusty"
dc_image_svnf =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VNF_21_3_1_Trusty"
dc_image_analytics = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/LB_images_terraform/providers/Microsoft.Compute/images/LB_images_terraform_LB_VOS_21_3_1_Bionic"
#DR_image_details
dr_image_director = "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/lab_dkumar_tf_resource_group/providers/Microsoft.Compute/images/lab_dkumar_tf_director_21.3.1_B"
dr_image_controller =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/lab_dkumar_tf_resource_group/providers/Microsoft.Compute/images/lab_dkumar_tf_controller_21.3.1_Trusty"
dr_image_svnf =  "/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/resourceGroups/lab_dkumar_tf_resource_group/providers/Microsoft.Compute/images/lab_dkumar_tf_controller_21.3.1_Trusty"

#DC variables
dc_location =  "eastus2"
dc_availability_zone =  "2"
dc_resource_group =  "dinz_tf_DC"
dc_vpc_address_space =  "10.153.0.0/16"
dc_newbits_subnet =  "8"
#### DC MGNT Subnet INFO ####
DC_mgmt_subnet = "10.153.0.0/24"
DC_mgmt_subnet_gateway = "10.153.0.1"
#["Master_Director","Analytics","Search"]
DC_dir_mgnt_interfaces_IP = ["10.153.0.21"]
DC_ana_mgnt_interfaces_IP = ["10.153.0.25","10.153.0.26","10.153.0.27","10.153.0.28"]
#[DC_router,Controller1]
DC_controller_flex_mgnt_interfaces_IP = ["10.153.0.22","10.153.0.23"]
#### DC SOUTH_BOUND Subnet INFO ####
DC_south_bound_network_subnet = "10.153.1.0/24"
dc_south_bound_network_gateway = "10.153.1.1"
#["Master_Director",DC_router]
DC_south_bound_network_interfaces_IP = ["10.153.1.21","10.153.1.22"]
#["Analytics","Search"]
DC_ana_south_bound_network_interfaces_IP = ["10.153.1.25","10.153.1.26","10.153.1.27","10.153.1.28"]
#### DC control network Subnet INFO ####
DC_control_network_subnet = "10.153.4.0/24"
#[DC_router,Controller1]
DC_control_network_interface_IP = ["10.153.4.22","10.153.4.23"]
#### DC INTER NETWORK Subnet INFO ####
DC_inter_network_subnet = "10.153.2.0/24"
DC_inter_network_subnet_gateway = "10.153.2.1"
#[DC_router]
DC_inter_network_interface_IP = ["10.153.2.22"]
#### DC INTERNET Subnet INFO ####
DC_internet_subnet = "10.153.3.0/24"
DC_internet_subnet_gateway = "10.153.3.1"
#[Controller1]
DC_internet_subnet_interface_IP = ["10.153.3.23","10.153.3.24"]
#DR variables
dr_location =  "Central India"
dr_availability_zone =  "1"
dr_resource_group =  "dinz_tf_DR"
dr_vpc_address_space =  "10.193.0.0/16"
dr_newbits_subnet =  "8"
#### DR MGNT Subnet INFO ####
DR_mgmt_subnet = "10.193.0.0/24"
DR_mgmt_subnet_gateway = "10.193.0.1"
#["Slave_Director"]
DR_dir_mgnt_interfaces_IP = ["10.193.0.21"]
#[DR_router,Controller2]
DR_controller_flex_mgnt_interfaces_IP = ["10.193.0.22","10.193.0.23"]
#### DR SOUTH_BOUND Subnet INFO ####
DR_south_bound_network_subnet = "10.193.1.0/24"
dr_south_bound_network_gateway = "10.193.1.1"
#["Slave_Director",DR_router]
DR_south_bound_network_interfaces_IP = ["10.193.1.21","10.193.1.22"]
#### DR control network Subnet INFO ####
DR_control_network_subnet = "10.193.4.0/24"
#[DR_router,Controller2]
DR_control_network_interface_IP = ["10.193.4.22","10.193.4.23"]
#### DR INTER NETWORK Subnet INFO ####
DR_inter_network_subnet = "10.193.2.0/24"
DR_inter_network_subnet_gateway = "10.193.2.1"
#[DR_router]
DR_inter_network_interface_IP = ["10.193.2.22"]
#### DR INTERNET Subnet INFO ####
DR_internet_subnet = "10.193.3.0/24"
DR_internet_subnet_gateway = "10.193.3.1"
#[Controller2]
DR_internet_subnet_interface_IP = ["10.193.3.23","10.193.3.24"]

#ORG NAME
parent_org_name = "versa"
overlay_prefixes = "10.0.0.0/8"

