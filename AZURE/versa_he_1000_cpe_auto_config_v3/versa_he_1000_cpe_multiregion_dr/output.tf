#CLI_LOGIN Details
output "Slave_Director_CLI_sshCommand" {
  value = "ssh -i ${var.private_key_name}.pem Administrator@${azurerm_public_ip.ip_dir[0].ip_address}"
}
# output "DR_Router_CLI_sshCommand" {
#   value = "ssh -i ${var.private_key_name}.pem admin@${azurerm_public_ip.ip_svnf_inet[0].ip_address}"
# }
# output "Versa_Controller-1_CLI_sshCommand" {
#   value = "ssh -i ${var.private_key_name}.pem admin@${azurerm_public_ip.ip_controller[0].ip_address}"
# }

#UI LOGIN DETAILS
output "Slave_Director_UI_Login" {
  value = "https://${azurerm_public_ip.ip_dir[0].ip_address}"
}

#Instance Name

output "Slave_Director_Instance" {
  value = azurerm_virtual_machine.directorVM[0].name
}
output "Versa_Controller_2_Instance" {
  value = azurerm_virtual_machine.controllerVM[0].name
}
output "DR_Router_Instance" {
  value = azurerm_virtual_machine.svnfVM[0].name
}

#Private_MGNT_IP
output "Slave_Director_MGMT_IP" {
  value = azurerm_network_interface.director_nic_1[0].private_ip_address
}
output "Versa_Controller_2_MGMT_IP" {
  value = azurerm_network_interface.controller_nic_1.private_ip_address
}
output "DR_Router_MGMT_IP" {
  value = azurerm_network_interface.svnf_nic_1.private_ip_address
}

#South Bound IP
output "Slave_Director_south_bound_IP" {
  value = azurerm_network_interface.director_nic_2[0].private_ip_address
}

#Public MGNT IP

output "Slave_Director_Public_IP" {
  value = azurerm_public_ip.ip_dir[0].ip_address
}
# output "Versa_Controller-1_Public_IP" {
#   value = azurerm_public_ip.ip_controller[0].ip_address
# }
# output "DR_Router_Public_IP" {
#   value = azurerm_public_ip.ip_svnf[0].ip_address
# }

# Internet Transport Public IP

output "DR_Router_Internet_Public_IP" {
  value = azurerm_public_ip.ip_svnf_inet[0].ip_address
}
output "Versa_Controller_2_InternetTransport_Public_IP" {
  value = azurerm_public_ip.ip_controller_inet[0].ip_address
}

# Internet Transport Private IP

output "Versa_Controller_2_Internet_Private_IP" {
  value = azurerm_network_interface.controller_nic_3.private_ip_address
}
output "DR_Router_Internet_private_IP" {
  value = azurerm_network_interface.svnf_nic_3.private_ip_address
}
output "DR_Router_south_bound_private_IP" {
  value = azurerm_network_interface.svnf_nic_2.private_ip_address
}
output "dr_router_control_ntw_ip" {
  value = azurerm_network_interface.svnf_nic_4.private_ip_address
}
output "Versa_Controller_2_south_bound_Private_IP" {
  value = azurerm_network_interface.controller_nic_2.private_ip_address
}
#Script Execution

output "DR_mgmt_subnet" {
  value = azurerm_subnet.mgmt_subnet.address_prefix
}
output "DR_south_bound_network_subnet" {
  value = azurerm_subnet.south_bound_network_subnet.address_prefix
}
output "DR_internet_subnet" {
  value = azurerm_subnet.wan_network_subnet.address_prefix
  # default = "10.153.1.0/24"
}