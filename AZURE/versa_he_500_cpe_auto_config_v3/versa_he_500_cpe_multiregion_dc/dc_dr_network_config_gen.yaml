#cloud-config
cloud_init_modules:
  - write-files
  - set_hostname
  - update_hostname
  - users-groups
  - ssh
hostname: DC-ROUTER
write_files:
- content: |
    interfaces {
        vni-0/0 {
            description South_Bound_Network;
            enable      true;
            unit 0 {
                description "South Bound Network connecting to Director and Analytics";
                enable      true;
                family {
                    inet {
                        address ${South_Bound_Network_ip}/24;
                    }
                }
            }
        }
        vni-0/1 {
            description Internet_Transport;
            enable      true;
            unit 0 {
                description "WAN Network connecting to Internet cloud";
                enable      true;
                family {
                    inet {
                        address ${internet_interface_ip}/24;
                    }
                }
            }
        }
        vni-0/2 {
            description Service_VNF_to_controller;
            enable      true;
            unit 0 {
                description "Service_VNF to controller connect";
                enable      true;
                family {
                    inet {
                        address ${dc_router_control_ntw_ip}/24;
                    }
                }
            }
        }
        tvi-0/1 {
            enable true;
            mtu    1400;
            mode   ipsec;
            type   ipsec;
            unit 0 {
                enable true;
                family {
                    inet {
                        address 192.168.0.1/30;
                    }
                }
            }
        }
    }
    networks {
        ipsec {
            interfaces [ vni-0/1.0 ];
        }
    }
    orgs {
        org ${parent_org_name} {
            available-routing-instances   [ Internet vpc-gateway ];
            owned-routing-instances       [ Internet vpc-gateway ];
            options {
                session-limit 1000000;
            }
            traffic-identification {
                using          [ tvi-0/1.0 vni-0/0.0 vni-0/2.0 ];
                using-networks [ ipsec ];
            }
            available-service-node-groups [ default-sng ];
        }
        org-services ${parent_org_name} {
            ipsec {
                vpn-profile transport-ipsec {
                    vpn-type                site-to-site;
                    local-auth-info {
                        auth-type psk;
                        id-type   ip;
                        key       versa1234;
                        id-string ${internet_interface_ip};
                    }
                    local {
                        address ${internet_interface_ip};
                    }
                    routing-instance        Internet;
                    tunnel-routing-instance vpc-gateway;
                    tunnel-initiate         automatic;
                    ipsec {
                        fragmentation pre-fragmentation;
                        force-nat-t   disable;
                        transform     esp-aes128-sha1;
                        mode          tunnel;
                        pfs-group     mod-none;
                        anti-replay   enable;
                        life {
                            duration 28800;
                        }
                    }
                    ike {
                        version     v2;
                        group       mod2;
                        transform   aes128-sha1;
                        lifetime    28800;
                        dpd-timeout 30;
                    }
                    peer-auth-info {
                        auth-type psk;
                        id-type   ip;
                        key       versa1234;
                        id-string ${DR_internet_private_IP};
                    }
                    peer {
                        address [ ${DR_internet_public_IP} ];
                    }
                    tunnel-interface        tvi-0/1.0;
                    hardware-accelerator    any;
                }
            }
            objects {
                zones {
                    trust;
                    untrust;
                    host;
                }
            }
        }
    }
    routing-instances {
        Internet {
            instance-type virtual-router;
            networks      [ ipsec ];
            routing-options {
                static {
                    route {
                        0.0.0.0/0 ${inter_network_subnet_gateway} none {
                            preference 1;
                        }
                    }
                }
            }
        }

        vpc-gateway {
            instance-type virtual-router;
            policy-options {
                redistribution-policy policy-1 {
                    term t1-direct {
                        match {
                            protocol direct;
                        }
                        action {
                                      accept;
                            set-origin igp;
                        }
                    }
                    term t1-static {
                        match {
                            protocol static;
                        }
                    }
                    term t2-bgp {
                        match {
                            protocol bgp;
                        }
                        action {
                                      accept;
                            set-origin igp;
                        }
                    }
                }
                redistribute-to-bgp policy-1;
            }
            interfaces    [ tvi-0/1.0 vni-0/0.0 vni-0/2.0 ];
            routing-options {
                static {
                    route {
                        ${DC_mgmt_subnet} ${dc_south_bound_network_gateway} none;
                    }
                }
            }            
            protocols {
                bgp {
                    64002 {
                        family {
                            inet {
                                unicast;
                            }
                        }
                        route-flap {
                            free-max-time    180;
                            reuse-max-time   60;
                            reuse-size       256;
                            reuse-array-size 1024;
                        }
                        graceful-restart {
                            maximum-restart-time 120;
                            recovery-time        120;
                            select-defer-time    120;
                            stalepath-time       120;
                        }
                        router-id ${South_Bound_Network_ip};
                        local-as {
                            as-number 64002;
                        }
                        group VPC {
                            type internal;
                            neighbor 192.168.0.2 {
                                local-address 192.168.0.1;
                                local-as      64002;
                                peer-as       64002;
                            }
                        }
                        group controller-1 {
                            type external;
                            neighbor ${controller_1_south_bound_ip} {
                                local-address ${dc_router_control_ntw_ip};
                                local-as      64002;
                                peer-as       64512;
                            }
                        }                        
                    }
                }
            }
        }
    }
    service-node-groups {
        default-sng {
            id       0;
            type     internal;
            services [ cgnat ipsec sdwan ];
        }
    }
    system {
        session {
            timeout-hard               0;
            timeout-udp                30;
            timeout-tcp                240;
            timeout-icmp               10;
            timeout-tcpwait            20;
            timeout-default            30;
            check-tcp-syn              false;
            reevaluate-reverse-flow    false;
            session-reevaluate         true;
            tcp-send-reset             true;
            tcp-secure-reset           false;
            tcp-adjust-mss {
                enable          true;
                interface-types all;
                mss             1200;
            }
            send-icmp-unreachable      false;
            allow-unsupported-protocol false;
            session-purge-batch-size   5;
            interim-update {
                disable false;
            }
        }
    } 
  path: /var/tmp/dc_dr_network_connect.cfg    
- content: |
    #!/bin/bash
    log_path="/etc/bootLog.txt"
    if [ -f "$log_path" ]
    then
        echo "Cloud Init script already ran earlier during first time boot.." >> $log_path
    else
        touch $log_path
    SSHKey="${sshkey}"
    KeyDir="/home/admin/.ssh"
    KeyFile="/home/admin/.ssh/authorized_keys"
    UBUNTU_RELEASE="$(lsb_release -cs)"
    echo "Starting cloud init script..." > $log_path

    echo "Modifying /etc/network/interface file.." >> $log_path
    cp /etc/network/interfaces /etc/network/interfaces.bak
    if [[ $UBUNTU_RELEASE == "trusty" ]]; then
    cat > /etc/network/interfaces << EOF
    # This file describes the network interfaces available on your system
    # and how to activate them. For more information, see interfaces(5).

    # The loopback network interface
    auto lo
    iface lo inet loopback

    # The primary network interface
    auto eth0
    iface eth0 inet dhcp

    EOF
    else
    cat > /etc/network/interfaces << EOF
    # This file describes the network interfaces available on your system
    # and how to activate them. For more information, see interfaces(5).

    # The loopback network interface
    auto lo
    iface lo inet loopback
    # The primary network interface
    auto eth0
    iface eth0 inet dhcp
        offload-gro off

    EOF
    fi
    echo -e "Modified /etc/network/interface file. Refer below new interface file content:\n`cat /etc/network/interfaces`" >> $log_path

    echo "Restart Network services.." >> $log_path
    if [[ $UBUNTU_RELEASE == "trusty" ]]; then
        /etc/init.d/networking restart >> /dev/null 2>&1
    else
        systemctl restart networking >> /dev/null 2>&1
    fi

    echo -e "Injecting ssh key into admin user.\n" >> $log_path
    if [ ! -d "$KeyDir" ]; then
        echo -e "Creating the .ssh directory and injecting the SSH Key.\n" >> $log_path
        sudo mkdir $KeyDir
        sudo echo $SSHKey >> $KeyFile
        sudo chown admin:versa $KeyDir
        sudo chown admin:versa $KeyFile
        sudo chmod 600 $KeyFile
    elif ! grep -Fq "$SSHKey" $KeyFile; then
        echo -e "Key not found. Injecting the SSH Key.\n" >> $log_path
        sudo echo $SSHKey >> $KeyFile
        sudo chown admin:versa $KeyDir
        sudo chown admin:versa $KeyFile
        sudo chmod 600 $KeyFile
    else
        echo -e "SSH Key already present in file: $KeyFile.." >> $log_path
    fi
    fi
  path: /var/tmp/initial_vos_cnf.sh
  permissions : '755'   
- content: |
    #!/bin/bash
    log_path="/var/tmp/vos_log.txt"
    if [ -f "$log_path" ]
    then
        echo "vos Init script already ran earlier during first time boot.." >> $log_path
    else
        touch $log_path
    echo -e "vsh service check" >> $log_path    
    source /etc/profile.d/versa-profile.sh
    result=$(vsh status)
    echo "$result" >> $log_path
    echo "$result" | egrep -qi 'failed|Stopped|not'
    until [ "$?" -ne 0 ]
    do
        echo "Versa Director services not running.." >> $log_path
        sleep 20
        result=$(vsh status)
        echo "$result" | egrep -qi 'failed|Stopped|not'
    done
    fi
    echo -e 'configure\nload merge /var/tmp/dc_dr_network_connect.cfg\ncommit' | /opt/versa/confd/bin/confd_cli -u admin -g admin >> $log_path
  path: /var/tmp/dc_dr_network_connect.sh
  permissions : '755'  
cloud_final_modules:
- runcmd
- scripts-user
runcmd:
- sed -i.bak "\$a\Match Address ${master_dir_mgmt_ip}/32\n  PasswordAuthentication yes\nMatch all" /etc/ssh/sshd_config
- sed -i.bak "\$a\Match Address ${master_dir_south_bound_ip}/32\n  PasswordAuthentication yes\nMatch all" /etc/ssh/sshd_config
- sed -i.bak "\$a\Match Address ${slave_dir_mgmt_ip}/32\n  PasswordAuthentication yes\nMatch all" /etc/ssh/sshd_config
- sed -i.bak "\$a\Match Address ${slave_dir_south_bound_ip}/32\n  PasswordAuthentication yes\nMatch all" /etc/ssh/sshd_config        
- sudo service ssh restart
- cd /var/tmp/
- ./initial_vos_cnf.sh 
- ./dc_dr_network_connect.sh 
- sudo service ssh restart
