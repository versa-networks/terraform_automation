This Terraform Template is intended to Automate and bringing up Versa Head End setup in one touch mode.
It will bring up 2 instances of Versa Director, 6 analytics node, 6 Log forwarder, DC service VNF , DR service VNF & Versa Controller's in different regions.

# Pre-requisites for using this template:

- **Terraform Install:** To Download & Install Terraform, refer Link "www.terraform.io/downloads.html"
- **Versa Head End Images:** Image available should available in region:
  - Versa Director
  - Versa Controller/service VNF
  - Versa Analytics


# Usage:

- Download all the files in PC where Terraform is installed. It is recommended that place all the files in folder as terraform will store the state file for this environment once it is applied.
- Go to the folder "versa_he_2500_cpe_auto_config_v3" where all the required files are placed.

- Use command `terraform init` to initialize. it will download necessary terraform plugins required to run this template.
- Then use command `terraform plan` to plan the deployment. It will show the plan regarding all the resources being provisioned as part of this template.
- At last use command `terraform apply` to apply this plan in action for deployment. It will start deploying all the resource on Azure.

The following figure illustrates the redundant (high availability) headend topology created by the Terraform template(s).

![2500_CPE_TOPOLOGY.png](./2500_CPE_TOPOLOGY.png)

It will require below files to run it.


|____output.tf
|____main.tf
|____2500_Topology.pdf
|____terraform.tfvars
|____vars.tf
|____versa_he_2500_cpe_multiregion_dc
| |____output.tf
| |____van.sh
| |____main.tf
| |____log_forwarder.sh
| |____dc_dr_network_config_gen.yaml
| |____controller.sh
| |____master_director.sh
| |____vars.tf
|____versa_he_2500_cpe_multiregion_dr
| |____output.tf
| |____main.tf
| |____dr_dc_network_config_gen.yaml
| |____slave_director.sh
| |____controller.sh
| |____vars.tf




**main.tf file:**

main.tf template file will perform below actions/activities when executed:

- It will deploy DC components (Master_Director,6-Node Analytics cluster and 6-Node log forwarder,DC service VNF, controller-1) in one region and deploy DR components (Slave_Director,DR service VNF,controller-2) in another region along with the IP details provide in terraform.tfvars.
- once instances deployed, Master_Director and Slave_Director will initialize the vnms-startup script in non-interactive mode
- DC and DR service VNF will initialize the cloud-init script along with BGP and IPSEC configuration,
- Master_Director and Slave_Director will form the HA,
- once HA is ready, then van_cluster_installer.py will start to integrate the analytics.
- Post van_cluster_installer.py DC service VNF, DR service VNF, controller-1 and Controller-2 will added to the Director.


**vars.tf file:**

vars.tf file will have definition for all the variables defined/used as part of this template. User does not need to update anything in this file.

**terraform.tfvars file:**

terraform.tfvars file is being used to get the variables values from user. User need to update the required fields according to their environment. This file will have below information:

###### common variables ######
- subscription_id : Provide the Subscription ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- client_id : Provide the Client ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- client_secret : Provide the Client Secret information here. This information will be obtained as part of terraform login done above under Pre-requisites step.
- tenant_id : Provide the Tenant ID information here. This information will be obtained as part of terraform login done above under Pre-requisites step.

###### file path example /Users/dinz/Downloads ######
private_key_name : Provide the key pair name Eg: "customer_name"
ssh_key : Provide the ssh public key information here. This key will be injected into instances which can be used to login into instances later on. Azure does not provide the option to generate the keys. User has to generate the ssh key using "sshkey-gen" or "putty key generator" tool to generate the ssh keys. Here Public key information is required. Eg: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3bi1jnbgPOVHq2d1Vu1lpc+i3HfRlCxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"

###### Host Name ######

dc_hostname_director : Provide the hostname for master Director instances
dr_hostname_director : Provide the hostname for slave Director instances
hostname_van : Provide the hostname for Analytics and search nodes Eg:["versa-analytics-1", "versa-analytics-2","versa-analytics-3", "versa-analytics-4","versa-search-1","versa-search-2"]
hostname_log_fwd : Provide the hostname for log forwarder instances Eg:["van-forwarder-01","van-forwarder-02","van-forwarder-03","van-forwarder-04","van-forwarder-05","van-forwarder-06"]
controller_1_hostname : Provide the hostname for controller-1 instances
controller_1_country : Provide the country info for controller-1 instances
controller_2_hostname : Provide the hostname for controller-2 instances
controller_2_country : Provide the country info for controller-2 instances

###### DC and DR INSTANCE TYPE ######

director_vm_size : Provide the instance type which will be used to provision the Versa Director Instance. By default, standard_F8s_v2 will be used.
analytics_vm_size : Provide the instance type which will be used to provision the Versa Analytics Instance. By default, standard_F8s_v2 will be used.
controller_vm_size : Provide the instance type which will be used to provision the Versa Controller Instance. By default, standard_F8s_v2 will be used.
svnf_vm_size : Provide the instance type which will be used to provision the Versa service VNF Instance. By default, standard_F8s_v2 will be used.

###### IMAGE'S ######
dc_image_director : Provide the Versa Director image for DC region
dc_image_analytics : Provide the Versa Analytics image for DC region
dc_image_controller/dc_image_svnf : Provide the Versa VOS image for DC region
dr_image_director : Provide the Versa Director image for DR region
dr_image_controller/dr_image_svnf : Provide the Versa VOS image for DR region

###### DC-DR CIDR Subnet INFO ######

DC_cidr_block : Provide the DC VPC subnet info. By default "10.153.0.0/16" will be created in DC.
DR_cidr_block : Provide the DR VPC subnet info. By default "10.193.0.0/16" will be created in DR.

###### DC MGNT Subnet INFO ######

DC_mgmt_subnet : Provide the DC management/North_bound subnet info. By default "10.153.0.0/24" will be created as management/North_bound subnet in DC.
DC_mgmt_subnet_gateway : Provide the DC management/North_bound gateway IP info. By default "10.153.0.1" will be created as management/North_bound gateway in DC.
DC_dir_mgnt_interfaces_IP : Provide the management IP details for ["Master_Director"]. By default "10.153.0.21" will be created respectively.
DC_ana_mgnt_interfaces_IP : Provide the management IP details for [6-Node Analytics cluster]. By default ["10.153.0.25","10.153.0.26","10.153.0.27","10.153.0.28","10.153.0.29","10.153.0.30"]will be created respectively.
DC_log_fwd_mgnt_interfaces_IP:Provide the management IP details for [6-Node log forwarder]. By default ["10.153.0.31","10.153.0.32","10.153.0.33","10.153.0.34","10.153.0.35","10.153.0.36"]will be created respectively.
DC_controller_flex_mgnt_interfaces_IP : Provide the management IP details for [DC_router,Controller-1,DC_DR_MGNT]. By default "10.153.0.22","10.153.0.23" will be created respectively.

#### DC SOUTH_BOUND Subnet INFO ####

DC_south_bound_network_subnet : Provide the DC south bound subnet info. By default "10.153.1.0/24" will be created as south bound subnet in DC.
dc_south_bound_network_gateway :  Provide the DC south bound gateway IP info. By default "10.153.1.1" will be created as south bound gateway in DC.
DC_south_bound_network_interfaces_IP : Provide the south bound IP details for ["Master_Director",DC_router]. By default "10.153.1.21","10.153.1.21"" will be created respectively.
DC_ana_south_bound_network_interfaces_IP : Provide the south bound IP details for [6-Node Analytics cluste]. By default "10.153.1.25","10.153.1.26","10.153.1.27","10.153.1.28","10.153.1.29","10.153.1.30" will be created respectively.
DC_log_fwd_south_bound_interfaces_IP : Provide the south bound IP details for [6-Node log forwarder]. By default 10.153.1.31,"10.153.1.32","10.153.1.33","10.153.1.34","10.153.1.35","10.153.1.36"will be created respectively.

#### DC CONTROL NETWORK Subnet INFO ####

DC_control_network_subnet : Provide the DC internet subnet info. By default "10.153.4.0/24" will be created as internet subnet in DC.
DC_control_network_interface_IP : Provide the DC internet IP details for [DC_router,Controller-1]. By default "10.153.4.22","10.153.4.23" will be created.

#### DC INTER NETWORK Subnet INFO ####

DC_inter_network_subnet : Provide the DC inter network subnet info. By default "10.153.2.0/24" will be created as inter network  subnet in DC.
DC_inter_network_subnet_gateway : Provide the DC inter network  gateway IP info. By default "10.153.2.1" will be created as inter network  gateway in DC.
DC_inter_network_interface_IP : Provide the DC inter network IP details for [DC_router]. By default "10.153.2.22" will be created.

#### DC INTERNET Subnet INFO ####

DC_internet_subnet : Provide the DC internet subnet info. By default "10.153.3.0/24" will be created as internet subnet in DC.
DC_internet_subnet_gateway : Provide the DC internet gateway IP info. By default "10.153.3.1" will be created as internet gateway in DC.
DC_internet_subnet_interface_IP : Provide the DC internet IP details for [Controller-1]. By default "10.153.3.23","10.153.3.24" will be created.

#### DR MGNT Subnet INFO ####

DR_mgmt_subnet : Provide the DR management/North_bound subnet info. By default "10.193.0.0/24" will be created as management/North_bound subnet in DR.
DR_mgmt_subnet_gateway : Provide the DR management/North_bound gateway IP info. By default "10.193.0.1" will be created as management/North_bound gateway in DR.
DR_dir_mgnt_interfaces_IP : Provide the management/North_bound IP details for ["Slave_Director"]. By default "10.193.0.21" will be created as slave Director management/North_bound.
DR_controller_flex_mgnt_interfaces_IP : Provide the management IP details for [DR_router,Controller2,DR_DC_MGNT]. By default "10.193.0.22","10.193.0.23", will be created respectively.

#### DR SOUTH_BOUND Subnet INFO ####

DR_south_bound_network_subnet : Provide the DR south bound subnet info. By default "10.193.1.0/24" will be created as south bound subnet in DR.
DR_internet_subnet_gateway : Provide the DR internet gateway IP info. By default "10.193.1.1" will be created as internet gateway in DR.
DR_south_bound_network_interfaces_IP : Provide the DR south bound IP details for [Slave_Director,DR_router,Controller2]. By default "10.193.1.21","10.193.1.22" will be created respectively.

#### DR CONTROL NETWORK Subnet INFO ####

DR_control_network_subnet : Provide the DR internet subnet info. By default "10.193.4.0/24" will be created as internet subnet in DR.
DR_control_network_interface_IP : Provide the internet IP details for [DR_router,Controller2]. By default "10.193.4.22","10.193.4.23" will be created.

#### DR INTER NETWORK Subnet INFO ####

DR_inter_network_subnet : Provide the DR inter network subnet info. By default "10.193.2.0/24" will be created as inter network  subnet in DR.
DR_inter_network_subnet_gateway : Provide the DR inter network  gateway IP info. By default "10.193.2.1" will be created as inter network  gateway in DR.
DR_inter_network_interface_IP : Provide the DR inter network IP details for [DR_router]. By default "10.193.2.22" will be created.

#### DR INTERNET Subnet INFO ####

DR_internet_subnet : Provide the DR internet subnet info. By default "10.193.3.0/24" will be created as internet subnet in DR.
DR_internet_subnet_gateway : Provide the DR internet gateway IP info. By default "10.193.3.1" will be created as internet gateway in DR.
DR_inter_network_interface_IP : Provide the internet IP details for [Controller2]. By default "10.193.3.23","10.193.3.24" will be created.

#### ORG NAME ####
parent_org_name = Provide the parent ORG name. By default "versa" will be used as parent ORG
overlay_prefixes = Provide the overlay prefixes . By default "10.0.0.0/8" will be used as overlay prefixes


**output.tf file:**

output.tf file will have information to provide the output
